const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const fs = require('fs-extra');

module.exports = function (defaults) {
  const app = new EmberApp(defaults, {
    'ember-cli-babel': { enableTypeScriptTransform: true },

    // Add options here
  });

  return app.toTree();
};
