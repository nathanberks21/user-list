# Ember Frontend

This project contains the Ember SPA. It only renders the "user-list" page but utilises the Stencil components and a service to handle adding / editing / removing users, and storing this on local storage.

If you'd like to reset the state please run `localStorage.clear()` within the dev tools console then refresh the page.
