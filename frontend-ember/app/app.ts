import Application from '@ember/application';
import { defineCustomElements } from '@nb21/components-stencil/loader';
import loadInitializers from 'ember-load-initializers';
import Resolver from 'ember-resolver';
import config from 'frontend-ember/config/environment';

defineCustomElements();

export default class App extends Application {
  modulePrefix = config.modulePrefix;

  podModulePrefix = config.podModulePrefix;

  Resolver = Resolver;
}

loadInitializers(App, config.modulePrefix);
