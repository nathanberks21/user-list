import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import type { FormSchema } from '@nb21/components-stencil/loader';

import type { User, UserFormData, UserStatus } from '../../types/user.types';
import type UserService from '../../services/user';
import { mapUserEnumToStatus } from 'frontend-ember/utils/user.utils';

const enum FormState {
  Add,
  Edit,
}

export default class UserListComponent extends Component {
  @service declare user: UserService;

  @tracked formSchema = this.getEmptyFormData();
  @tracked formState = FormState.Add;
  @tracked rowIndexBeingEdited: number | undefined;

  private resetFormState(): void {
    this.rowIndexBeingEdited = undefined;
    this.formState = FormState.Add;
    this.formSchema = this.getEmptyFormData();
  }

  private getEmptyFormData(user?: User): FormSchema {
    return new Map([
      [
        'firstName',
        {
          type: 'string',
          label: 'First name',
          value: user?.firstName ?? '',
          minLength: 2,
          maxLength: 50,
          required: true,
        },
      ],
      [
        'lastName',
        {
          type: 'string',
          label: 'Last name',
          value: user?.lastName ?? '',
          minLength: 2,
          maxLength: 50,
          required: true,
        },
      ],
      [
        'email',
        {
          type: 'string',
          label: 'Email',
          value: user?.email ?? '',
          minLength: 5,
          maxLength: 320,
          required: true,
        },
      ],
      [
        'status',
        {
          type: 'string',
          label: 'Status',
          value: mapUserEnumToStatus(user?.status) ?? 'Active',
          enum: ['Active', 'Inactive', 'Disabled'],
        },
      ],
    ]);
  }

  get formTitle(): 'Edit User' | 'Add User' {
    if (this.formState === FormState.Edit) {
      return 'Edit User';
    }

    return 'Add User';
  }

  get formButtonText(): 'Edit' | 'Add' {
    if (this.formState === FormState.Edit) {
      return 'Edit';
    }

    return 'Add';
  }

  get isEditingDisabledStatusUser(): boolean {
    return (
      this.rowIndexBeingEdited != null &&
      this.user.users[this.rowIndexBeingEdited]?.status === 'disabled'
    );
  }

  @action handleFormSubmit(ev: CustomEvent<UserFormData>): void {
    if (this.formState === FormState.Add) {
      this.user.addUser(ev.detail);
    } else if (
      this.formState === FormState.Edit &&
      this.rowIndexBeingEdited != null
    ) {
      this.user.editUser(this.rowIndexBeingEdited, ev.detail);
    }

    this.resetFormState();
  }

  @action convertDateToMonthDayYear(jsDateString?: string): string {
    if (!jsDateString) {
      return '–';
    }

    const date = new Date(jsDateString);
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const year = date.getFullYear();

    return `${month}/${day}/${year}`;
  }

  @action isDisabled(status: UserStatus): boolean {
    return status === 'disabled';
  }

  @action mapStatusEnumToText(
    status: UserStatus,
  ): 'Active' | 'Inactive' | 'Disabled' {
    switch (status) {
      case 'active':
        return 'Active';
      case 'inactive':
        return 'Inactive';
      case 'disabled':
        return 'Disabled';
    }
  }

  @action getStatusIconSrc(status: UserStatus): string {
    switch (status) {
      case 'active':
        return 'icons/check.svg';
      case 'inactive':
        return 'icons/circle.svg';
      case 'disabled':
        return 'icons/close.svg';
    }
  }

  @action enableAddUser(): void {
    this.resetFormState();
  }

  @action enableEditUser(user: User, idx: number): void {
    this.rowIndexBeingEdited = idx;
    this.formSchema = this.getEmptyFormData(user);
    this.formState = FormState.Edit;
  }

  @action deleteUser(email: string, idx: number): void {
    if (!confirm(`Are you sure you want to delete user ${email}?`)) {
      return;
    }

    this.formSchema = this.getEmptyFormData();
    this.formState = FormState.Add;

    this.user.deleteUser(idx);
  }
}
