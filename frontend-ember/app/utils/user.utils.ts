import type { UserStatus } from '../types/user.types';

export function mapUserStatusToEnum(
  status: 'Active' | 'Inactive' | 'Disabled',
): UserStatus {
  switch (status) {
    case 'Active':
      return 'active';
    case 'Inactive':
      return 'inactive';
    case 'Disabled':
      return 'disabled';
  }
}

export function mapUserEnumToStatus(
  status?: UserStatus,
): 'Active' | 'Inactive' | 'Disabled' | undefined {
  switch (status) {
    case 'active':
      return 'Active';
    case 'inactive':
      return 'Inactive';
    case 'disabled':
      return 'Disabled';
  }
}

export function convertDateToMonthDayYear(jsDateString: string) {
  const date = new Date(jsDateString);
  const month = String(date.getMonth() + 1).padStart(2, '0');
  const day = String(date.getDate()).padStart(2, '0');
  const year = date.getFullYear();

  // Return the formatted date string
  return `${month}/${day}/${year}`;
}
