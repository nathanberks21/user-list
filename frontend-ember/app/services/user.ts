import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import type { User, UserFormData } from '../types/user.types';
import { mapUserStatusToEnum } from 'frontend-ember/utils/user.utils';

const USERS_LOCAL_STORAGE_KEY = 'users';

export default class UserService extends Service {
  @tracked users: User[] = JSON.parse(
    localStorage.getItem(USERS_LOCAL_STORAGE_KEY) || 'null',
  ) || [
    {
      firstName: 'John',
      lastName: 'Schmitt',
      email: 'johnschmitt@example.com',
      status: 'active',
      created: 'Thu Jan 05 2020 12:28:09 GMT-0500 (Eastern Standard Time)',
      updated: 'Thu Jan 05 2020 13:33:29 GMT-0500 (Eastern Standard Time)',
    },
    {
      firstName: 'Beth',
      lastName: 'Forest',
      email: 'bethforest@example.com',
      status: 'inactive',
      created: 'Fri June 06 2021 15:01:07 GMT-0500 (Eastern Standard Time)',
      updated: 'Fri June 06 2022 15:01:07 GMT-0500 (Eastern Standard Time)',
    },
    {
      firstName: 'Edward',
      lastName: 'Jones',
      email: 'edwardjones@example.com',
      status: 'disabled',
      created: 'Tue Sept 24 2022 16:45:11 GMT-0500 (Eastern Standard Time)',
      updated: 'Mon Nov 03 2022 11:12:45 GMT-0500 (Eastern Standard Time)',
    },
  ]; // initial dataset to demo, this would otherwise be set to `[]`

  private saveUsers() {
    localStorage.setItem(USERS_LOCAL_STORAGE_KEY, JSON.stringify(this.users));

    this.users = [...this.users];
  }

  addUser(user: UserFormData) {
    const status = mapUserStatusToEnum(user.status);

    this.users.push({
      ...user,
      status,
      created: new Date().toString(),
    });

    this.saveUsers();
  }

  editUser(index: number, updatedUser: UserFormData) {
    const status = mapUserStatusToEnum(updatedUser.status);

    const existingUser = this.users[index];

    if (!existingUser) {
      throw `User with index ${index} does not exist`;
    }

    this.users[index] = {
      ...existingUser,
      ...updatedUser,
      status,
      updated: new Date().toString(),
    };

    this.saveUsers();
  }

  deleteUser(index: number) {
    this.users.splice(index, 1);
    this.saveUsers();
  }
}

declare module '@ember/service' {
  interface Registry {
    user: UserService;
  }
}
