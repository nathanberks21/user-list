export type User = {
  firstName: string;
  lastName: string;
  email: string;
  status: UserStatus;
  created: string;
  updated?: string;
};

export type UserFormData = {
  firstName: string;
  lastName: string;
  email: string;
  status: 'Disabled' | 'Active' | 'Inactive';
};

export type UserStatus = 'active' | 'inactive' | 'disabled';
