module.exports = {
  $schema: 'http://json.schemastore.org/prettierrc',
  arrowParens: 'avoid',
  bracketSpacing: true,
  bracketSameLine: false,
  jsxSingleQuote: false,
  quoteProps: 'consistent',
  semi: true,
  singleQuote: true,
  tabWidth: 2,
  trailingComma: 'all',
  useTabs: false,
};
  
  