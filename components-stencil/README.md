# Stencil Components

These Stencil components serve as a brief example of what a design system using native web components could look like.

The CSS variables names & props aren't fully thought through; I'd expect this to be solidified more in a real-world environment where more collaboration would be involved.

This enables components to be easily imported within the EmberJS SPA and promotes re-usability with a framework agnostic approach.
