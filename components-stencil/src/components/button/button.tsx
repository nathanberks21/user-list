import {
  Component,
  h,
  Prop,
  Event,
  EventEmitter,
  AttachInternals,
} from '@stencil/core';

type ButtonColour = 'primary' | 'secondary';

@Component({
  tag: 'app-button',
  styleUrl: 'button.css',
  formAssociated: true,
  shadow: true,
})
export class AppButton {
  @Prop() colour: ButtonColour = 'primary';
  @Prop() isDisabled?: boolean;
  @Prop() buttonIconSrc?: string;
  @Prop() buttonIconAlt?: string;
  @Prop() type?: string;
  @Prop() ariaLabel: string;

  @Event() buttonClick: EventEmitter;

  @AttachInternals() internals: ElementInternals;

  handleClickOrKeyUp(): void {
    if (this.isDisabled) {
      return;
    }

    this.buttonClick.emit();

    this.internals.form?.requestSubmit();
  }

  render() {
    return (
      <button
        class={{ [this.colour]: true }}
        type={this.type}
        disabled={this.isDisabled}
        aria-label={this.ariaLabel}
        onClick={() => this.handleClickOrKeyUp()}
        onKeyUp={ev =>
          (ev.key === 'Enter' || ev.key === ' ') && this.handleClickOrKeyUp()
        }
      >
        {!!this.buttonIconSrc && (
          <img src={this.buttonIconSrc} alt={this.buttonIconAlt} />
        )}
        <slot />
      </button>
    );
  }
}
