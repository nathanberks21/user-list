import { Component, h, Host } from '@stencil/core';

@Component({
  tag: 'app-table',
  styleUrl: 'table.css',
  shadow: true,
})
export class AppTable {
  render() {
    return (
      <Host>
        <table>
          <tbody>
            <slot />
          </tbody>
        </table>
      </Host>
    );
  }
}
