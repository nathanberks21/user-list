import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-table-row',
  styleUrl: 'table-row.css',
  shadow: true,
})
export class AppTableRow {
  render() {
    return (
      <tr>
        <slot />
      </tr>
    );
  }
}
