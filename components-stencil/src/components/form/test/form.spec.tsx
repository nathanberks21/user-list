import { h } from '@stencil/core';
import { newSpecPage } from '@stencil/core/testing';
import { AppForm } from '../form';
import { FormSchema } from '../../../types/form.types';

describe('app-form', () => {
  const formSchema: FormSchema = new Map([
    [
      'name',
      {
        type: 'string',
        label: 'Name',
        value: '',
        required: true,
        minLength: 2,
      },
    ],
    ['email', { type: 'string', label: 'Email', value: '', required: true }],
    [
      'country',
      {
        type: 'string',
        label: 'Country',
        value: '',
        enum: ['England', 'Scotland', 'Wales', 'Northern Ireland'],
      },
    ],
  ]);

  it('should emit formSubmit event with form values on submit', async () => {
    const page = await newSpecPage({
      components: [AppForm],
      template: () => <app-form formSchema={formSchema} />,
    });

    const formSubmitSpy = jest.fn();
    page.root.addEventListener('formSubmit', formSubmitSpy);

    page.rootInstance.formValues = {
      name: 'Jane Doe',
      email: 'jane@example.com',
      country: 'England',
    };

    const nameInputElement = page.root.shadowRoot.querySelector(
      'app-input[name="name"]',
    );
    nameInputElement.dispatchEvent(
      new CustomEvent('inputChange', { detail: 'Jane Doe' }),
    );

    const emailInputElement = page.root.shadowRoot.querySelector(
      'app-input[name="email"]',
    );
    emailInputElement.dispatchEvent(
      new CustomEvent('inputChange', { detail: 'jane@example.com' }),
    );

    const countrySelectElement = page.root.shadowRoot.querySelector(
      'app-select[name="country"]',
    );
    countrySelectElement.dispatchEvent(
      new CustomEvent('valueChange', { detail: 'Northern Ireland' }),
    );

    const formElement = page.root.shadowRoot.querySelector('form');
    formElement.dispatchEvent(new Event('submit', { cancelable: true }));

    expect(formSubmitSpy).toHaveBeenCalled();
    expect(formSubmitSpy.mock.calls[0][0].detail).toEqual({
      name: 'Jane Doe',
      email: 'jane@example.com',
      country: 'Northern Ireland',
    });
  });

  it('should validate form and prevent submission if invalid', async () => {
    const page = await newSpecPage({
      components: [AppForm],
      template: () => <app-form formSchema={formSchema} />,
    });

    const formSubmitSpy = jest.fn();
    page.root.addEventListener('formSubmit', formSubmitSpy);

    await page.waitForChanges();

    const formElement = page.root.shadowRoot.querySelector('form');
    formElement.dispatchEvent(new Event('submit', { cancelable: true }));

    expect(formSubmitSpy).not.toHaveBeenCalled();

    await page.waitForChanges();

    expect(page.root.shadowRoot.querySelector('.error-message')).not.toBeNull();
  });

  it('should clear validation errors when input changes', async () => {
    const page = await newSpecPage({
      components: [AppForm],
      template: () => <app-form formSchema={formSchema} />,
    });

    const formElement = page.root.shadowRoot.querySelector('form');
    formElement.dispatchEvent(new Event('submit', { cancelable: true }));

    expect(page.rootInstance.invalidPropertyKeys.has('name')).toBe(true);

    const inputElement = page.root.shadowRoot.querySelector(
      'app-input[name="name"]',
    );
    inputElement.dispatchEvent(
      new CustomEvent('inputChange', { detail: 'Jane Doe' }),
    );

    expect(page.rootInstance.invalidPropertyKeys.has('name')).toBe(false);
  });
});
