import {
  Component,
  h,
  Prop,
  State,
  Event,
  EventEmitter,
  VNode,
  Watch,
} from '@stencil/core';
import { FormProperty, FormSchema, FormValues } from '../../types/form.types';

@Component({
  tag: 'app-form',
  styleUrl: 'form.css',
  shadow: true,
})
export class AppForm {
  @Prop() formSchema: FormSchema;
  @Prop() submitButtonText = 'Submit form';
  @Prop() isReadOnly = false;

  @State() formValues: FormValues = {};
  @State() invalidPropertyKeys = new Set();

  @Event() formSubmit: EventEmitter<FormValues>;

  @Watch('formSchema')
  onFormSchemaChange() {
    this.mapFormSchemaToValues();
  }

  componentWillLoad() {
    this.mapFormSchemaToValues();
  }

  mapFormSchemaToValues(): void {
    this.formValues = [...this.formSchema.entries()].reduce(
      (formValues, [key, { value, enum: enumOptions }]) => {
        formValues[key] = value;

        if (enumOptions?.length && !value) {
          formValues[key] = enumOptions[0];
        }

        return formValues;
      },
      {},
    );
  }

  submitForm(ev: Event): void {
    ev.preventDefault();

    this.validateForm();

    if (this.invalidPropertyKeys.size > 0) {
      return;
    }

    this.formSubmit.emit(this.formValues);
  }

  // TODO get form association (https://stenciljs.com/docs/form-associated)
  // working to prevent duplicated validation & better accessibility
  validateForm() {
    [...this.formSchema.entries()].forEach(([key, formProperty]) => {
      const enteredValue = this.formValues[key];

      if (
        formProperty.minLength != null &&
        (enteredValue == null || enteredValue.length < formProperty.minLength)
      ) {
        this.invalidPropertyKeys.add(key);
      }

      if (
        formProperty.maxLength != null &&
        (enteredValue == null || enteredValue.length > formProperty.maxLength)
      ) {
        this.invalidPropertyKeys.add(key);
      }

      if (formProperty.required && enteredValue == null) {
        this.invalidPropertyKeys.add(key);
      }
    });

    this.invalidPropertyKeys = new Set(this.invalidPropertyKeys);
  }

  clearValidationPropertyKey(propertyKey: string) {
    this.invalidPropertyKeys.delete(propertyKey);

    this.invalidPropertyKeys = new Set(this.invalidPropertyKeys);
  }

  renderSelect(propertyKey: string, formProperty: FormProperty): VNode {
    return (
      <app-select
        name={propertyKey}
        options={formProperty.enum}
        selectedValue={this.formValues[propertyKey] ?? formProperty.enum[0]}
        label={formProperty.label}
        isReadOnly={this.isReadOnly}
        onValueChange={({ detail }) => (this.formValues[propertyKey] = detail)}
      />
    );
  }

  renderInput(propertyKey: string, formProperty: FormProperty): VNode {
    return (
      <app-input
        name={propertyKey}
        value={this.formValues[propertyKey]}
        label={formProperty.label}
        isInvalid={this.invalidPropertyKeys.has(propertyKey)}
        isRequired={formProperty.required}
        minLength={formProperty.minLength}
        maxLength={formProperty.maxLength}
        isReadOnly={this.isReadOnly}
        onInputChange={({ detail }) => {
          this.formValues[propertyKey] = detail;

          this.clearValidationPropertyKey(propertyKey);
        }}
      />
    );
  }

  renderFormElements(): VNode[] {
    return [...this.formSchema.entries()].map(([key, formProperty]) => {
      if (formProperty.enum) {
        return this.renderSelect(key, formProperty);
      }

      return this.renderInput(key, formProperty);
    });
  }

  renderErrorMessage(): VNode | undefined {
    if (!this.invalidPropertyKeys.size) {
      return;
    }

    // TODO add more specific error messages
    return (
      <div class="error-message">
        Please ensure all form elements are entered correctly.
      </div>
    );
  }

  render() {
    return (
      <form onSubmit={ev => this.submitForm(ev)}>
        {this.renderFormElements()}
        {this.renderErrorMessage()}
        <app-button type="submit" isDisabled={this.isReadOnly}>
          {this.submitButtonText}
        </app-button>
      </form>
    );
  }
}
