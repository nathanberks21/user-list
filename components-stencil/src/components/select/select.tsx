import { Component, h, Prop, Event, EventEmitter, Host } from '@stencil/core';

@Component({
  tag: 'app-select',
  styleUrl: 'select.css',
  shadow: true,
})
export class AppSelect {
  @Prop() options: string[] = [];
  @Prop() selectedValue?: string;
  @Prop() name?: string;
  @Prop() label: string;
  @Prop() ariaLabel?: string;
  @Prop() isReadOnly = false;

  @Event() valueChange: EventEmitter<string>;

  handleChange(ev: Event) {
    const selectElement = ev.target as HTMLSelectElement;

    this.valueChange.emit(selectElement.value);
  }

  render() {
    return (
      <Host>
        <label htmlFor={this.name}>{this.label}</label>
        <select
          id={this.name}
          aria-label={this.ariaLabel}
          disabled={this.isReadOnly}
          onInput={ev => this.handleChange(ev)}
        >
          {this.options.map(option => (
            <option value={option} selected={option === this.selectedValue}>
              {option}
            </option>
          ))}
        </select>
      </Host>
    );
  }
}
