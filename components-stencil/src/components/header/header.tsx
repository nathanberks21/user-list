import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-header',
  styleUrl: 'header.css',
  shadow: true,
})
export class AppHeader {
  render() {
    return (
      <h1>
        <slot />
      </h1>
    );
  }
}
