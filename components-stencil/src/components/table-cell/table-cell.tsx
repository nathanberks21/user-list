import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-table-cell',
  styleUrl: 'table-cell.css',
  shadow: true,
})
export class AppTableCell {
  render() {
    return (
      <td>
        <slot />
      </td>
    );
  }
}
