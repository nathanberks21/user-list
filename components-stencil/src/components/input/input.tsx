import {
  Component,
  h,
  Prop,
  Event,
  EventEmitter,
  Host,
  VNode,
} from '@stencil/core';

@Component({
  tag: 'app-input',
  styleUrl: 'input.css',
  shadow: true,
})
export class AppInput {
  @Prop() value: string;
  @Prop() placeholder?: string;
  @Prop() name?: string;
  @Prop() label: string;
  @Prop() ariaLabel?: string;
  @Prop({ reflect: true }) isInvalid = false;
  @Prop() isRequired?: boolean;
  @Prop() minLength?: number;
  @Prop() maxLength?: number;
  @Prop() isReadOnly = false;

  @Event() inputChange: EventEmitter<string>;

  handleInput(ev: Event) {
    const inputElement = ev.target as HTMLInputElement;

    this.inputChange.emit(inputElement.value);
  }

  renderRequiredIndicator(): VNode | undefined {
    if (!this.isRequired) {
      return;
    }

    return <span>*</span>;
  }

  render() {
    return (
      <Host>
        <label htmlFor={this.name}>
          {this.label}
          {this.renderRequiredIndicator()}
        </label>
        <input
          id={this.name}
          type="text"
          placeholder={this.placeholder}
          aria-label={this.ariaLabel}
          value={this.value}
          required={this.isRequired}
          minLength={this.minLength}
          maxLength={this.maxLength}
          readOnly={this.isReadOnly}
          onInput={ev => this.handleInput(ev)}
        />
      </Host>
    );
  }
}
