import { newSpecPage } from '@stencil/core/testing';
import { AppInput } from '../input';

describe('app-input', () => {
  it('should emit inputChange event on input', async () => {
    const page = await newSpecPage({
      components: [AppInput],
      html: `<app-input label="Foo Label" value="initial"></app-input>`,
    });

    const inputElement = page.root.shadowRoot.querySelector('input');
    const inputChangeSpy = jest.fn();
    page.root.addEventListener('inputChange', inputChangeSpy);

    inputElement.value = 'new value';
    inputElement.dispatchEvent(new Event('input'));

    expect(inputChangeSpy).toHaveBeenCalled();
    expect(inputChangeSpy.mock.calls[0][0].detail).toBe('new value');
  });

  it('should reflect isInvalid property', async () => {
    const page = await newSpecPage({
      components: [AppInput],
      html: `<app-input label="Foo Label" isInvalid></app-input>`,
    });

    expect(page.root.getAttribute('isInvalid')).not.toBe(null);
  });

  it('should render required indicator if isRequired is true', async () => {
    const page = await newSpecPage({
      components: [AppInput],
      html: `<app-input label="Foo Label" is-required></app-input>`,
    });

    expect(page.root.shadowRoot.querySelector('span')).not.toBeNull();
  });

  it('should not render required indicator if isRequired is false', async () => {
    const page = await newSpecPage({
      components: [AppInput],
      html: `<app-input label="Foo Label"></app-input>`,
    });

    expect(page.root.shadowRoot.querySelector('span')).toBeNull();
  });
});
