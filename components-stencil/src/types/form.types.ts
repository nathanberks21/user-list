export type FormSchema = Map<string, FormProperty>;

export type FormProperty = {
  type: 'string';
  label: string;
  value?: string;
  minLength?: number;
  maxLength?: number;
  enum?: string[];
  required?: boolean;
};

export type FormValues = {
  [formKey: string]: string;
};
