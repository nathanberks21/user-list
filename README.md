# User List Project

## Overview

This project has been set up as a monorepo example project, which StencilJS re-usable components + Ember SPA. Commands can be run from route to start dev mode for with the Stencil components or the Ember project.

## Development

After cloning the project.

```bash
cd user-list
npm i
npm run build.stencil
npm start
```

This will start the Ember project, running on `localhost:4200`

## Testing

I've included a few test examples:

- `components-stencil/src/components/button/test/button.e2e.ts`
- `components-stencil/src/components/form/test/form.spec.tsx`
- `components-stencil/src/components/input/test/input.spec.ts`

Substantially more testing would be required but time constraints are limiting my ability to add more right now.

I've also not included Ember tests as I'd need to spend more time familiarising myself with the framework. Therefore, the `npm run test.ember` is not currently working.

## Improvements

I've built the project to match the designs + requirements. Given the designs I'd raise a few questions prior to implementation, and given time constraints I've left a few bits of functionality out, but I've documented some of this below:

- Form errors should be more specific, so the user knows which input to update and how to fix any issues
- Utilise native form validation rather than replicating it in the form component
- Table could have column headers to make things more concise - e.g., "created" and "updated" text adds unnecessary noise, removed if headers existed
- Make the "remove" icon a "danger" colour + add a better confirmation modal
- Add form validation to check if the user email already exists
- Add form validation to check the entered email address is in the correct format
- The form & table (aside & main) could be separate components as they became quite large
- Animate the aside to show / hide when editing or adding a user
- Dropdown component should be able to map the enum to the textual values, e.g. `active` => `Active` to prevent having to do that in multiple areas to get the values I want
- Lots more unit tests
