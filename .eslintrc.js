module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  plugins: ['unused-imports', 'import', 'unicorn'],
  extends: [
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:ember/recommended',
  ],
  parserOptions: {
    project: ['./*/tsconfig.json'],
    sourceType: 'module',
  },
  root: true,
  rules: {
    'prettier/prettier': 'error',
    'import/first': 'error',
    'import/order': [
      'error',
      {
        'alphabetize': {
          order: 'asc',
          caseInsensitive: false,
        },
        'groups': [['builtin', 'external'], 'parent', ['sibling', 'index']],
        'newlines-between': 'always',
      },
    ],
    'quote-props': ['error', 'consistent'],
    'import/newline-after-import': 'error',
    'import/no-duplicates': 'error',
    'import/no-mutable-exports': 'error',
    'unused-imports/no-unused-imports': [
      'error',
      {
        varsIgnorePattern: '^h$',
      },
    ],
    'prefer-object-spread': 'warn',
    'max-params': ['warn', 5],
    'camelcase': 'warn',
    'new-cap': [
      'error',
      {
        capIsNewExceptionPattern: '@*',
      },
    ],
    'no-lonely-if': 'error',
    'no-trailing-spaces': [
      'error',
      {
        skipBlankLines: true,
      },
    ],
    'lines-around-comment': 'error',
    'spaced-comment': ['error', 'always'],
    'class-methods-use-this': [
      'warn',
      {
        exceptMethods: [],
      },
    ],
    'no-console': [
      'error',
      {
        allow: ['warn', 'error', 'debug'],
      },
    ],
    'no-unneeded-ternary': 'error',
    'no-else-return': 'error',
    'no-param-reassign': 'error',
    'no-return-await': 'error',
    'no-useless-call': 'error',
    'no-useless-concat': 'error',
    'prefer-promise-reject-errors': 'error',
    'prefer-regex-literals': 'error',
    'vars-on-top': 'error',
    'no-whitespace-before-property': 'error',
    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'never',
        prev: ['singleline-const', 'singleline-let', 'singleline-var'],
        next: ['singleline-const', 'singleline-let', 'singleline-var'],
      },
      {
        blankLine: 'always',
        prev: '*',
        next: 'return',
      },
      {
        blankLine: 'always',
        prev: ['block', 'block-like'],
        next: '*',
      },
      {
        blankLine: 'always',
        prev: '*',
        next: ['block', 'block-like'],
      },
      {
        blankLine: 'always',
        prev: ['const', 'let', 'var'],
        next: 'expression',
      },
      {
        blankLine: 'always',
        prev: 'expression',
        next: ['const', 'let', 'var'],
      },
      {
        blankLine: 'always',
        prev: 'expression',
        next: 'expression',
      },
      {
        blankLine: 'always',
        prev: '*',
        next: 'break',
      },
      {
        blankLine: 'always',
        prev: [
          'block',
          'block-like',
          'expression',
          'function',
          'class',
          'const',
          'let',
          'var',
        ],
        next: ['export', 'import'],
      },
      {
        blankLine: 'always',
        prev: ['export', 'import'],
        next: [
          'block',
          'block-like',
          'expression',
          'function',
          'class',
          'const',
          'let',
          'var',
        ],
      },
      {
        blankLine: 'any',
        prev: ['export'],
        next: ['export'],
      },
      {
        blankLine: 'always',
        prev: ['import'],
        next: ['export'],
      },
      {
        blankLine: 'always',
        prev: ['export'],
        next: ['import'],
      },
    ],
    'unicorn/no-useless-undefined': [
      'error',
      {
        checkArguments: false,
      },
    ],
    'arrow-body-style': ['error', 'as-needed'],
    'no-restricted-syntax': ['error', 'Literal[value=/\\.\\.\\./]'],
    'import/no-extraneous-dependencies': 0,
    'object-curly-newline': 0,
    'import/no-unresolved': [
      'error',
      {
        ignore: ['^@ember/.+'],
      },
    ],
  },
  overrides: [
    {
      files: ['**/*.{ts,tsx}'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: ['**/tsconfig.json', '**/tsconfig.node.json'],
        sourceType: 'module',
      },
      plugins: ['@typescript-eslint'],
      extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/stylistic',
        'plugin:import/typescript',
      ],
      rules: {
        '@typescript-eslint/consistent-type-definitions': ['error', 'type'],
        '@typescript-eslint/no-explicit-any': 'warn',
        '@typescript-eslint/explicit-module-boundary-types': [
          'error',
          {
            allowArgumentsExplicitlyTypedAsAny: true,
            allowedNames: ['render'],
          },
        ],
        '@typescript-eslint/array-type': 'error',
        '@typescript-eslint/consistent-type-assertions': 'error',
        '@typescript-eslint/consistent-type-imports': 'error',
        '@typescript-eslint/prefer-for-of': 'error',
        '@typescript-eslint/prefer-optional-chain': 'error',
        '@typescript-eslint/no-shadow': 'error',
        '@typescript-eslint/no-use-before-define': [
          'error',
          {
            functions: false,
            classes: false,
            variables: false,
          },
        ],
        '@typescript-eslint/no-unused-vars': [
          'error',
          {
            vars: 'all',
            args: 'after-used',
            ignoreRestSiblings: false,
            argsIgnorePattern: '^_',
            varsIgnorePattern: '^h$',
          },
        ],
        '@typescript-eslint/no-empty-function': 'error',
        '@typescript-eslint/no-throw-literal': 'error',
        '@typescript-eslint/no-unused-expressions': [
          'error',
          {
            allowShortCircuit: true,
            allowTernary: true,
          },
        ],
        '@typescript-eslint/require-await': 'warn',
        '@typescript-eslint/lines-between-class-members': [
          'error',
          'always',
          {
            exceptAfterSingleLine: true,
          },
        ],
        '@typescript-eslint/naming-convention': [
          'warn',
          {
            selector: 'variable',
            format: ['camelCase', 'PascalCase', 'UPPER_CASE'],
            leadingUnderscore: 'allow',
          },
          {
            selector: 'variable',
            types: ['boolean'],
            format: ['PascalCase', 'UPPER_CASE'],
            prefix: [
              'is',
              'should',
              'has',
              'can',
              'did',
              'will',
              'does',
              'was',
              'do',
              'are',
              'IS_',
              'SHOULD_',
              'HAS_',
              'CAN_',
              'DID_',
              'WILL_',
              'DOES_',
              'WAS_',
              'DO_',
              'ARE_',
            ],
          },
        ],
        '@typescript-eslint/no-extra-semi': 'error',
        '@typescript-eslint/semi': 'error',
      },
    },
    {
      files: ['*.spec.{ts,tsx}'],
      rules: {
        'import/order': 'off',
        'import/first': 'off',
      },
    },
    // nodejs config overrides
    {
      files: ['.eslintrc.js'],
      env: {
        browser: false,
        node: true,
        commonjs: true,
      },
    },
  ],
  ignorePatterns: ['**/public/*.js'],
};
